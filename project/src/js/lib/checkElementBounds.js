export default (entirelyInBounds, element, windowHeight) => {

  // entirelyInBounds -> 0 = 'in screen... return true'
  // entirelyInBounds -> 50 = 'top passed half of screen, return true'
  // entirelyInBounds -> 100 = 'completely in screen... no other element visible... return true'

  let
    firstCompareValue = 0,
    secondCompareValue = windowHeight;

  if (entirelyInBounds === 0) {
    firstCompareValue = windowHeight;
    secondCompareValue = 0;
  } else if (entirelyInBounds === 50) {
    firstCompareValue = secondCompareValue = windowHeight / 2;
  }

  const {top, bottom} = element.getBoundingClientRect();

  if (top < firstCompareValue && bottom > secondCompareValue) return true;
  return false;
};
