const loadDeferredStyles = () => {
  const href = document.documentElement.dataset.cssHref;
  const deferredStyles = document.createElement(`link`);
  deferredStyles.setAttribute(`id`, `deferred-styles`);
  deferredStyles.setAttribute(`rel`, `stylesheet`);
  deferredStyles.setAttribute(`type`, `text/css`);
  deferredStyles.setAttribute(`href`, href);
  document.head.append(deferredStyles);
};

const raf =
  requestAnimationFrame ||
  mozRequestAnimationFrame ||
  webkitRequestAnimationFrame ||
  msRequestAnimationFrame;

if (raf) {
  raf(loadDeferredStyles);
} else {
  window.addEventListener(`load`, loadDeferredStyles);
}
