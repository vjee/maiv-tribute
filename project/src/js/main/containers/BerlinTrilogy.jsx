import React from 'react';
import {PropTypes as MPropTypes, observer} from 'mobx-react';

import TrilogyAlbum from './../components/TrilogyAlbum';

const BerlinTrilogy = ({store}) => {

  return (
    <div className='berlin-trilogy__react-root'>
      <TrilogyAlbum store={store} song={0} tag='low' name='Low' />
      <TrilogyAlbum store={store} song={1} tag='heroes' name='"Heroes"' />
      <TrilogyAlbum store={store} song={2} tag='lodger' name='Lodger' />
    </div>
  );

};

BerlinTrilogy.propTypes = {
  store: MPropTypes.observableObject.isRequired
};

export default observer(BerlinTrilogy);
