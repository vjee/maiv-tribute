import React from 'react';
import {PropTypes as MPropTypes, observer} from 'mobx-react';

import Bookmark from './../components/Bookmark';

const ProgressBar = ({store}) => {

  const {progress, bookmarks} = store;

  const styles = {
    progressBar: {
      width: `${progress}%`
    }
  };

  const handleBookmarkCLick = anchor => {
    const $card = document.getElementsByClassName(anchor)[0];
    const {top} = $card.getBoundingClientRect();
    document.body.scrollTop += top;
  };

  return (
    <div>
      <div
        className='progress-bar__progress'
        style={styles.progressBar}
      />
      {bookmarks.map(bookmark => (
        <Bookmark
          key={bookmark.key}
          {...bookmark}
          bookmarkClickHandler={handleBookmarkCLick}
        />
      ))}
    </div>
  );

};

ProgressBar.propTypes = {
  store: MPropTypes.observableObject.isRequired
};

export default observer(ProgressBar);
