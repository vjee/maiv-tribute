import React from 'react';
import {PropTypes as MPropTypes, observer} from 'mobx-react';

import PlayPauseButton from './../components/PlayPauseButton.jsx';
import StopButton from './../components/StopButton.jsx';

const MusicPlayer = ({store}) => {

  const songTitle = store.songs[store.currentSong].title;

  const handleTogglePlay = () => {
    if (store.isPlaying) store.pause();
    else store.play();
  };

  const handleStop = () => {
    if (store.isPlaying) store.pause(true);
  };

  return (
    <div className='music-player__controlls'>
      <div className='music-player__left'>
        <div className='music-player__controlls__title'>{songTitle}</div>
      </div>
      <div className='music-player__right'>
        <PlayPauseButton
          currentAction={store.isPlaying ? `pause` : `play`}
          actionTogglePlay={handleTogglePlay}
        />
        <StopButton
          actionStop={handleStop}
        />
      </div>
    </div>
  );

};

MusicPlayer.propTypes = {
  store: MPropTypes.observableObject.isRequired
};

export default observer(MusicPlayer);
