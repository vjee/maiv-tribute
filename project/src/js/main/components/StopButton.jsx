import React, {PropTypes} from 'react';

const StopButton = ({actionStop}) => {

  return (
    <button
      type='button'
      className='button music-player__controlls__button music-player__controlls__button_stop'
      onClick={actionStop}
    >
      <svg className='music-player__controlls__icon' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 500 500'>
        <rect x='105' y='106' width='289' height='289' />
      </svg>
    </button>
  );

};

StopButton.propTypes = {
  actionStop: PropTypes.func.isRequired
};

export default StopButton;
