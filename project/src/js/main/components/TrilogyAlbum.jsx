import React, {PropTypes} from 'react';
import {PropTypes as MPropTypes, observer} from 'mobx-react';

const TrilogyAlbum = ({store, song, tag, name}) => {

  const handleAlbumClick = event => {
    const album = event.currentTarget;
    const songId = parseInt(album.dataset.songId);

    if (store.currentSong === songId && store.isPlaying) {
      store.pause();
    } else {
      store.setSong(songId);
      if (!store.isPlaying) store.play();
    }
  };

  let card = (
    <div className='card__artwork__album__hover'>
      <svg className='card__artwork__album__icon' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 500 500'>
        <polygon className='card__artwork__album__icon__fill' points='359 249 141 394 141 105 359 249 359 249' />
      </svg>
    </div>
  );

  if (store.currentSong === song && store.isPlaying) {
    card = (
      <div className='card__artwork__album__playing'>
        <svg className='card__artwork__album__icon' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 500 500'>
          <rect className='card__artwork__album__icon__fill' x='124' y='106' width='93' height='289' />
          <rect className='card__artwork__album__icon__fill' x='283' y='106' width='93' height='289' />
        </svg>
      </div>
    );
  }

  return (
    <figure
      className='card__artwork__album'
      data-song-id={song}
      onClick={handleAlbumClick}
    >
      <picture>
        <source type='image/webp' srcSet={`assets/img/album-covers/${tag}.webp`} />
        <img
          className='card__artwork__image'
          src={`assets/img/album-covers/${tag}.jpg`}
          alt={`album cover, David Bowie - ${name}`}
        />
      </picture>
      {card}
      <figcaption className='card__artwork__title'>{name}</figcaption>
    </figure>
  );

};

TrilogyAlbum.propTypes = {
  store: MPropTypes.observableObject.isRequired,
  song: PropTypes.number.isRequired,
  tag: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};

export default observer(TrilogyAlbum);
