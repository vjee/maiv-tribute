import React, {PropTypes} from 'react';

const Bookmark = ({title, position, passed, anchor, hover, bookmarkClickHandler}) => {

  const passedClass = passed ? `passed` : ``;
  const bookmarkType = anchor !== `null` ? `` : `small`;
  const hoverClass = hover ? `hover` : ``;

  const styles = {
    bookmark: {
      left: `${position}%`
    }
  };

  const handleBookmarkClick = () => {
    if (anchor !== `null`) bookmarkClickHandler(anchor);
  };

  return (
    <div
      className={`progress-bar__bookmark ${hoverClass} ${passedClass} ${bookmarkType}`}
      style={styles.bookmark}
      onClick={handleBookmarkClick}>
      <div className='progress-bar__label'>{title}</div>
    </div>
  );

};

Bookmark.propTypes = {
  title: PropTypes.string.isRequired,
  anchor: PropTypes.string.isRequired,
  position: PropTypes.number.isRequired,
  hover: PropTypes.bool.isRequired,
  passed: PropTypes.bool.isRequired,
  bookmarkClickHandler: PropTypes.func.isRequired
};

export default Bookmark;
