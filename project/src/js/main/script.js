/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import {render} from 'react-dom';

import ProgressBar from './containers/ProgressBar';
import MusicPlayer from './containers/MusicPlayer';
import BerlinTrilogy from './containers/BerlinTrilogy';
import StoreProgressBar from './stores/ProgressBar';
import StoreMusicPlayer from './stores/MusicPlayer';
import checkElementBounds from './../lib/checkElementBounds';
import {add as classAdd, remove as classRemove} from './../lib/className';

const $progressBar = document.getElementsByClassName(`progress-bar`)[0];
const $musicPlayer = document.getElementsByClassName(`music-player`)[0];
const $berlinTrilogy = document.getElementsByClassName(`berlin-trilogy`)[0];
const $cards = document.getElementsByClassName(`card`);
const $scrollCards = document.getElementsByClassName(`card_scroll`);
const $autoPlayVideos = document.getElementsByClassName(`autoplay-video`);
const $wrapper = document.getElementsByClassName(`wrapper`)[0];
const $menuButton = document.getElementsByClassName(`menu-button`)[0];
const $menu = document.getElementsByClassName(`menu`)[0];
const $menuItems = document.getElementsByClassName(`menu__list__item`);
const $carouselCard = $cards[4];
const $cardCardArtwork = $carouselCard.getElementsByClassName(`card__artwork__carousel`)[0];
const carouselImageCount = 15;
const cardThemes = [`blue-yellow-white`, `hidden`, `beige-orange-black`, `blue-yellow-white`, `yellow-red-black`, `blue-yellow-white`, `orange-yellow-white`, `beige-orange-black`];
const scrollBarThemes = [`beige`, `hidden`, `purple`, `blue`, `red`, `blue`, `orange`, `orange`];
const menuButtonThemes = [`orange`, `hidden`, `orange`, `blue`, `yellow`, `yellow`, `orange`, `yellow`];

let carouselCardHeight = $carouselCard.getBoundingClientRect().height;
let menuOpened = false;
let documentHeight = document.documentElement.clientHeight;
let documentWidth = document.documentElement.clientWidth;
let currentCarouselImage = 0;
let carouselTimeout = null;

const raf =
  requestAnimationFrame ||
  mozRequestAnimationFrame ||
  webkitRequestAnimationFrame ||
  msRequestAnimationFrame;

const init = () => {

  window.addEventListener(`scroll`, handleScroll);

  window.addEventListener(`resize`, () => {
    documentHeight = document.documentElement.clientHeight;
    carouselCardHeight = $carouselCard.getBoundingClientRect().height;
  });

  if (documentWidth >= 1080) {
    render(<ProgressBar store={StoreProgressBar} />, $progressBar);
  }

  if (documentWidth >= 624) {
    render(<MusicPlayer store={StoreMusicPlayer} />, $musicPlayer);
  }

  render(<BerlinTrilogy store={StoreMusicPlayer} />, $berlinTrilogy);

  preloadHeroesImages();

  $menuButton.addEventListener(`click`, toggleMenu);

  for (let i = 0;i < $menuItems.length;i ++) {
    $menuItems[i].addEventListener(`click`, goToLink);
  }
};

const goToLink = event => {
  const $link = event.currentTarget;
  const targetClass = $link.dataset.targetClass;
  const $target = document.getElementsByClassName(targetClass)[0];
  const {top} = $target.getBoundingClientRect();
  document.body.scrollTop += top;
  toggleMenu();
};

const toggleMenu = () => {
  if (menuOpened) {
    classAdd($menu, `hidden`);
    classRemove($menuButton, `menu-button_open`);
  } else {
    classRemove($menu, `hidden`);
    classAdd($menuButton, `menu-button_open`);
  }

  menuOpened = !menuOpened;
};

const preloadHeroesImages = () => {
  for (let i = 0;i < carouselImageCount;i ++) {
    const img = document.createElement(`img`);
    img.src = `assets/img/heroes/heroes-${i}.png`;
  }
};

const scrollHeader = scrollTop => {
  if (scrollTop < $cards[0].getBoundingClientRect().height) {
    $cards[0].style.top = `-${scrollTop / 40}rem`;
  } else {
    $cards[0].style.top = 0;
  }
};

const imageCarousel = scrollTop => {
  const run = checkElementBounds(100, $carouselCard, documentHeight);
  if (run) {
    const cardScrollableHeight = carouselCardHeight - documentHeight;
    const ratio = cardScrollableHeight / carouselImageCount;
    const relativeScrollTop = scrollTop - $carouselCard.offsetTop;

    const heroesImageNumber = Math.min(carouselImageCount - 1, Math.floor(relativeScrollTop / ratio));
    $cardCardArtwork.style.backgroundImage = `url(assets/img/heroes/heroes-${heroesImageNumber}.png)`;
  }
};

const imageCarouselMobile = () => {
  const run = checkElementBounds(0, $carouselCard, documentHeight);
  if (run) {
    if (currentCarouselImage < carouselImageCount - 1) currentCarouselImage ++;
    else currentCarouselImage = 0;
    $cardCardArtwork.style.backgroundImage = `url(assets/img/heroes/heroes-${currentCarouselImage}.png)`;

    carouselTimeout = setTimeout(() => {
      raf(imageCarouselMobile);
      clearInterval(carouselTimeout);
      carouselTimeout = null;
    }, 500);
  }
};

const toggleFixedClasses = () => {
  for (let i = 0;i < $scrollCards.length;i ++) {
    const {top, bottom} = $scrollCards[i].getBoundingClientRect();
    if (top <= 0) {
      classAdd($scrollCards[i], `card_fixed`);
      if (bottom <= document.documentElement.clientHeight) {
        classAdd($scrollCards[i], `card_fixed_bottom`);
      } else {
        classRemove($scrollCards[i], `card_fixed_bottom`);
      }
      if (bottom <= 0) {
        classRemove($scrollCards[i], `card_fixed`);
        classRemove($scrollCards[i], `card_fixed_bottom`);
      }
    } else {
      classRemove($scrollCards[i], `card_fixed`);
    }
  }
};

const toggleThemes = () => {
  for (let i = 0;i < $cards.length;i ++) {
    const run = checkElementBounds(50, $cards[i], document.documentElement.clientHeight);
    if (run) {
      cardThemes.forEach(theme => classRemove($wrapper, `music-player_theme_${theme}`));
      scrollBarThemes.forEach(theme => classRemove($wrapper, `progress-bar_theme_${theme}`));
      menuButtonThemes.forEach(theme => classRemove($wrapper, `menu-button_theme_${theme}`));
      classAdd($wrapper, `music-player_theme_${cardThemes[i]}`);
      classAdd($wrapper, `progress-bar_theme_${scrollBarThemes[i]}`);
      classAdd($wrapper, `menu-button_theme_${menuButtonThemes[i]}`);
    }
  }
};

const videoToggler = () => {
  for (let i = 0;i < $autoPlayVideos.length;i ++) {
    const run = checkElementBounds(0, $autoPlayVideos[i], document.documentElement.clientHeight);
    if (run) {
      if ($autoPlayVideos[i].paused) $autoPlayVideos[i].play();
    } else {
      if (!$autoPlayVideos[i].paused) $autoPlayVideos[i].pause();$autoPlayVideos[i].currentTime = 0;
    }
  }
};

const handleScroll = () => {
  const scrollTop = document.body.scrollTop;
  documentWidth = document.documentElement.clientWidth;

  if (documentWidth >= 1080) {
    scrollHeader(scrollTop);
  }

  if (documentWidth >= 920) {
    toggleFixedClasses();
    imageCarousel(scrollTop);
  } else {
    if (!carouselTimeout) raf(imageCarouselMobile);
  }

  videoToggler();
  toggleThemes();
};

window.addEventListener(`load`, init);
