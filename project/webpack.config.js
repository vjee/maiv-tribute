const path = require(`path`);
const webpack = require(`webpack`);
const {HotModuleReplacementPlugin} = webpack;
const {UglifyJsPlugin} = webpack.optimize;
const ExtractTextWebpackPlugin = require(`extract-text-webpack-plugin`);
const {getIfUtils, removeEmpty} = require(`webpack-config-utils`);
const {ifProduction, ifDevelopment} = getIfUtils(process.env.NODE_ENV);
const DashboardPlugin = require(`webpack-dashboard/plugin`);

const publicPath = `/jasper.van.gestel/20162017/ma4/tribute/`;
// const publicPath = `/remco.van.gestel/20162017/ma4/tribute/`;
// const publicPath = `/`;

const port = 3000;

const config = {
  entry: {
    main: removeEmpty([
      ifDevelopment(`./src/css/critical/style.css`),
      `./src/css/main/style.css`,
      `./src/js/main/script.js`
    ]),
    critical: removeEmpty([
      ifProduction(`./src/css/critical/style.css`)
    ]),
    loadDeferredStyles: removeEmpty([
      ifProduction(`./src/js/load-deferred-styles/script.js`)
    ])
  },
  resolve: {
    extensions: [`.js`, `.jsx`, `.css`]
  },
  output: {
    path: path.join(__dirname, `dist`),
    filename: `js/[name].js`,
    publicPath
  },
  devtool: `source-map`,
  devServer: {
    contentBase: `./src`,
    hot: true,
    overlay: {
      errors: true,
      warnings: true
    },
    port
  },
  module: {
    rules: removeEmpty([
      ifDevelopment({
        test: /\.css$/,
        use: [
          {loader: `style-loader`},
          {loader: `css-loader`, options: {importLoaders: 1}},
          {loader: `postcss-loader`}
        ]
      }),
      {
        test: /\.(jsx?)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: `babel-loader`
          },
          {
            loader: `eslint-loader`,
            options: {
              fix: true
            }
          }
        ]
      },
      {
        test: /\.(svg|png|jpe?g|gif|webp)$/,
        loader: `url-loader`,
        options: {
          limit: 1000, // inline if < 1 kb
          context: `./src`,
          name: `[path][name].[ext]`
        }
      },
      {
        test: /\.(mp3|mp4|wav)$/,
        loader: `file-loader`,
        options: {
          context: `./src`,
          name: `[path][name].[ext]`
        }
      },
      ifProduction({
        test: /\.(svg|png|jpe?g|gif)$/,
        loader: `image-webpack-loader`,
        enforce: `pre`
      })
    ])
  },
  plugins: removeEmpty([
    ifDevelopment(
      new HotModuleReplacementPlugin()
    ),
    ifDevelopment(
      new DashboardPlugin()
    ),
    ifProduction(
      new UglifyJsPlugin({
        sourceMap: true,
        comments: false
      })
    )
  ])
};

if (process.env.NODE_ENV === `production`) {
  console.log(`building for '${publicPath}'`);
  for (const key in config.entry) {
    const regString = `(${key}\/.+\.css)`;
    const loader = new ExtractTextWebpackPlugin(`css/${key}.css`);
    config.module.rules.push({
      test: new RegExp(regString),
      loader: loader.extract([`css-loader?importLoaders=1`, `postcss-loader`])
    });
    config.plugins.push(loader);
  }
}

module.exports = config;
