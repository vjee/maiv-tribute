# Consult 05
---


## Voor het gesprek

- development1: enkele images in de css laden niet
- development2: images die veranderen a.d.h.v. scrollpositie
- development3: 'longread'-manier van scrollen proberen te implementeren


## Tijdens het gesprek

- development1: nooit absolute paden gebruiken in je css
- development2: gewerkt met Math.floor() om preciezer de te tonen foto aan te geven
- development3: wanneer een kaart de bottom van het scherm bereikt, deze op fixed zetten, en de volgende kaart een margin-top van dezelfde hoogte geven


## Na het gesprek

- development1: absolute paden verwijderen
- development2: gevonden oplossing verder uitwerken
- development3: uitwerken als er tijd genoeg is
