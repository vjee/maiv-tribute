# Consult 03
---


## Voor het gesprek

- tekst goed/genoeg/teveel?
- ideeen voor design goed/voldoende?
- interactie scroll-bar goed?


## Tijdens het gesprek

- teveel tekst
- meer toespoitsen op onderwerp (bv. types)
- meer interactie (klikken, slepen, droppen)
- meer design gericht
- niet gewoon foto's gebruiken zonder bewerking

## Na het gesprek

- content moet definitief zijn
- grafische bepaling / sfeer bepalen (adhv content)
