# Consult 05
---


## Voor het gesprek

- nood aan een algemene feedback op ons design


## Tijdens het gesprek

- 'Berlin era': iets meer duiding proberen maken, eventueel een jaartal of periode
- beeldmateriaal: eventueel een caption erbij plaatsen, zodat het duidelijk is waar de foto over gaat zonder dat de gebruiker de hele tekst moet lezen
- Berlijn moet duidelijker naar voren komen in het verhaal. Eventueel door react onderdelen
- op 'thin white duke' meer doen met het beeldmateriaal
- in platte tekst meer met tussentitels werken om het de gebruiker niet te zwaar te maken + anders wordt het te droog
- de webpagina van Nostalgie meer ontdekken om leuke verwijzingen te maken
- commercieel aspect van onze onepager meer bekijken
- nostalgie verwijzing kan op laatste pagina, en daar mag ook beeldmateriaal bij
- titels kunnen eventueel met duotone (pinterest)
- fonts zijn oké en mooi
- het is duidelijk wat we willen doen
- apart quote scherm is niet goed, vooral omdat het iets eenmaligs was


## Na het gesprek

- captions bij al het beeldmateriaal plaatsen
- Berlijn meer aanwezig maken in alle pagina's (bv. kaart, berlin wall of tv-toren lichtjes zichtbaar)
- 'Berlin era': iets meer duiding proberen maken, eventueel een jaartal of periode
- meer commerciele verwijzingen maken naar Radio Nostalgie
- typografie verfijnen: arceren, kerning en tussentitels in platte tekst
- 
